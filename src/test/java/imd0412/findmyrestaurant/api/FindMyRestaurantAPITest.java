package imd0412.findmyrestaurant.api;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import imd0412.findmyrestaurant.domain.GeoCoordinate;
import imd0412.findmyrestaurant.domain.Restaurant;
import imd0412.findmyrestaurant.domain.Review;
import imd0412.findmyrestaurant.service.IMapService;
import imd0412.findmyrestaurant.service.IRestaurantService;

public class FindMyRestaurantAPITest {
	
	private  static IMapService mapService;
	private  static IRestaurantService restaurantService;
	public static Restaurant restaurant;
	private static GeoCoordinate geoCoordinate;
	
	@BeforeClass
	public static void config() {

		restaurant = new Restaurant("Sal e Brasa", "Av. Salgado Filho, 1900.", "Rodizio de carnes");
		geoCoordinate = new GeoCoordinate(2.0, 2.0);
		mapService = Mockito.mock(IMapService.class);
		restaurantService = Mockito.mock(IRestaurantService.class);
		Mockito.when(mapService.convertToGeographicCoordinates(Mockito.any(String.class))).thenReturn(geoCoordinate);
	}
	
	@Test
	public final void testNearRestaurants_WithStubs() {
		
		Mockito.when(restaurantService.getReviews("Sal e Brasa"))
				.thenReturn(Arrays.asList(new Review("Muito bom", restaurant, 5)));
		Mockito.when(restaurantService.findNearRestaurants(geoCoordinate)).thenReturn(Arrays.asList(restaurant));

		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapService, restaurantService);

		// INPUTS
		String speciality = "Rodizio de carnes";
		String address = "Av. Salgado Filho, 1900.";

		List<Restaurant> nearRestaurants = api.findNearRestaurantsBySpeciality(address, speciality);
		Assert.assertFalse(nearRestaurants.isEmpty());
		Assert.assertEquals(nearRestaurants.get(0).getName(), "Sal e Brasa");
	}
	
	@Test
	public final void testNearRestaurants_WithStubs_EmptyList() {
		

		Mockito.when(mapService.convertToGeographicCoordinates(Mockito.any(String.class))).thenReturn(geoCoordinate);
		Mockito.when(restaurantService.getReviews("Sal e Brasa"))
				.thenReturn(Arrays.asList(new Review("Muito bom", restaurant, 5)));
		Mockito.when(restaurantService.findNearRestaurants(geoCoordinate)).thenReturn(Arrays.asList(restaurant));

		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapService, restaurantService);

		// INPUTS
		String speciality = "Rodizio de pizza";
		String address = "Av. Salgado Filho, 1700.";

		List<Restaurant> nearRestaurants = api.findNearRestaurantsBySpeciality(address, speciality);

		Assert.assertEquals(nearRestaurants.isEmpty(),true);
	}
	
	@Test
	public final void testGetFrequentWordsInReviews_WithStubs() {
		
		Mockito.when(restaurantService.getReviews("Sal e Brasa"))
				.thenReturn(Arrays.asList(new Review("Muito bom, Muito Legal", restaurant, 5)));

		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapService, restaurantService);

		Map<String, Integer> frequentWordsInReview = api.getFrequentWordsInReviews("Sal e Brasa");
		Assert.assertFalse(frequentWordsInReview.isEmpty());
		Assert.assertEquals(new Integer(2), frequentWordsInReview.get("Muito"));
	}
	
	@Test
	public final void testGetFrequentWordsInReviews_WordNotFound_WithStubs() {
		
		Mockito.when(restaurantService.getReviews("Sal e Brasa"))
				.thenReturn(Arrays.asList(new Review("Muito bom, Muito Legal", restaurant, 5)));

		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapService, restaurantService);

		Map<String, Integer> frequentWordsInReview = api.getFrequentWordsInReviews("Sal e Brasa");
		Assert.assertFalse(frequentWordsInReview.isEmpty());
		Assert.assertEquals(null, frequentWordsInReview.get("Barato"));
	}
	
	@Test
	public final void testGetFrequentWordsInReviews_RestaurantNull_WithStubs() {
		
		Mockito.when(restaurantService.getReviews("Sal e Brasa"))
				.thenReturn(Arrays.asList(new Review("Muito bom, Muito Legal", restaurant, 5)));

		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapService, restaurantService);

		Map<String, Integer> frequentWordsInReview = api.getFrequentWordsInReviews("Sal e Brasawret");

		Assert.assertEquals(null, frequentWordsInReview.get("Muito"));
	}	

}
